from django.urls import path
from . import views

app_name= 'library'

urlpatterns = [
    path('', views.home),
    # path('liked/<slug:title>/<slug:authors>/<slug:publisher>/<slug:publishedDate>', views.like)
    path('liked/<int:id>',views.like),  
]