from django.db import models

# Create your models here.
class Book(models.Model):
    title = models.CharField(max_length=200)
    authors = models.CharField(max_length=200, null=True)
    publisher = models.CharField(max_length=100, null=True)
    published_date = models.CharField(max_length=100, null=True)
    like_count = models.IntegerField()

    def __str__(self):
        return self.title
